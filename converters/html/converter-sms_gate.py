#!/usr/bin/env python3

# This is free and unencumbered software released into the public domain.
# See LICENSE file for details.

import locale
import re
import os
import logging
from pathlib import Path
import json
import datetime
import xml.etree.ElementTree as ET


LOGGER = logging.getLogger(__name__)


class Converter:
    def import_data(self, path):
        files = os.listdir(path)

        phone_numbers = set()
        for fd in files:
            phone_number = os.path.splitext(fd.split()[-1])[0]
            phone_number = re.sub(r'^\+1', '', phone_number)
            phone_numbers.add(phone_number)

        self.data = dict()
        for i in phone_numbers:
            self.data[i] = list()

        for fd in files:
            phone_number = os.path.splitext(fd.split()[-1])[0]
            phone_number = re.sub(r'^\+1', '', phone_number)

            date = datetime.datetime.strptime(fd.split()[0], "%Y%m%dT%H%M%S")

            with open(path + '/' + fd, encoding='utf-8') as f:
                content = f.readlines()

            content = "".join(content[2:])
            is_received = '↓' in fd
            data = (date, is_received, content)
            self.data[phone_number].append(data)

    def convert(self, outpath):
        seen = set()

        def convert_conversation(addr):
            conversation = self.data[addr]
            outfile = Path(outpath, f"{addr}.html")
            if outfile in seen:
                raise FileExistsError(f"oops, {outfile} has already been used")
            seen.add(outfile)

            hconv = self.build_conversation(conversation)

            html = ET.Element('html')
            hhead = ET.SubElement(html, 'head')
            ET.SubElement(hhead, 'link', rel='stylesheet', href='dark.css')
            ET.SubElement(hhead, 'link', rel='stylesheet', href='style.css')
            hbody = ET.SubElement(html, 'body')
            hbody.append(hconv)
            with outfile.open('wb') as fd:
                fd.write(ET.tostring(html, method='html'))

            LOGGER.info('done conversation %r', addr)

        for addr in self.data:
            try:
                convert_conversation(addr)
            except Exception as exc:
                LOGGER.exception('could not convert a conversation: %s', exc)

    def build_conversation(self, jconv):
        hconv = ET.Element('div', **{
            'itemscope': 'itemscope',
            'itemtype': 'http://schema.org/Message',
        })

        for jmsg in sorted(jconv, key=lambda jconv: jconv[0]):
            self.build_sms(jmsg, hconv)

        return hconv


    def build_sms(self, jmsg, hconv):
        is_received = jmsg[1]
        dt = jmsg[0]

        hmsg = ET.SubElement(
            hconv, 'div', id=str(jmsg[0]),
            **{
                'class': f'message message-{"received" if is_received else "sent"}',
                'itemscope': 'itemscope',
                'itemprop': 'hasPart',
                'itemtype': 'http://schema.org/Message',
            },
        )

        # haddr = ET.SubElement(
        #     hmsg, 'div', **{
        #         'class': 'message-address',
        #         'itemprop': 'sender' if is_received else 'recipient',
        #     })
        # haddr.text = jmsg['address']

        htime = ET.SubElement(
            hmsg, 'time', **{
                'class': 'message-date',
                'itemprop': 'dateReceived',
                'datetime': dt.isoformat(),
            })
        htime.text = dt.strftime('%Y-%m-%d %H:%M:%S')

        hbody = ET.SubElement(hmsg, 'div', **{'class': 'message-body'})
        hbody.text = jmsg[2]

        LOGGER.debug('done sms %r', jmsg[0])


def main():
    import argparse

    locale.setlocale(locale.LC_ALL, '')

    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    parser.add_argument('-o', '--output-dir', default='.')
    parser.add_argument('-v', '--verbose', action='store_true')
    args = parser.parse_args()

    levels = {
        False: logging.INFO,
        True: logging.DEBUG,
    }
    logging.basicConfig(
        level=levels[args.verbose],
        format='%(message)s'
    )

    c = Converter()
    c.import_data(args.file)
    c.convert(args.output_dir)

if __name__ == '__main__':
    main()
